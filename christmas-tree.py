#!/usr/bin/python
 
import RPi.GPIO as GPIO
import time
from datetime import datetime
from random import randint
 
# GPIO outputs in order of LED numbers (Star is last)
LEDS = [4,15,13,21,25,8,5,10,16,17,27,26,24,9,12,6,20,19,14,18,11,7,23,22,2]
TOTAL= len(LEDS)
 
# Variable used if not in December
PULSE = 0
DECREASE = 0
CLEAN = 0
 
# Set all GIOPs and PWMs
def setup():
    global pwm
    pwm = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    for x in range(0,TOTAL):
      GPIO.setup(LEDS[x], GPIO.OUT)
      pwm[x] = GPIO.PWM(LEDS[x], 50)
      pwm[x].start(0)
 
# Used in December and limit to 25 after Christmas Day
def dayofdec():
  x = datetime.today().day
  if x >= 26:
    x = 25
  return(x)
 
setup()
 
while True:
  # Get month
  MONTH = datetime.today().month
  # If Month is December start build Christmas tree
  if MONTH == 12:
    # Start from 1st day with 1 LED and build up to 25
    for x in range(0,dayofdec()):
      # For first 24 LEDs have full range of PWM
      if x <= 23:
        pwm[x].ChangeDutyCycle(randint(25,100))
      # For the Star no flame effect
      else:
        pwm[x].ChangeDutyCycle(100)
 
    time.sleep(0.1)
  # If not in December pulsate the Star
  else:
    if CLEAN == 0:
      setup()
      CLEAN = 1
 
 
    pwm[24].ChangeDutyCycle(PULSE)
    time.sleep(0.1)
    # Decrease or increase pulse value
    if DECREASE == 0:
      PULSE += 10
    else:
      PULSE -= 10
    # If max value change direction
    if PULSE == 100:
      DECREASE = 1
    # If min value change direction
    if PULSE == 0:
      DECREASE = 0